#!/bin/bash 

fast=/opt/NGS/fastqc/FastQC_v0.11.5/fastqc
inputdir=/data1/workspace/DCI/secord/RNASeq/Data/Reihani_3520_160802A1
outdir=/data1/workspace/DCI/secord/RNASeq/QC/fastqc/

for n in {2..13}
do
    m=$((${n}+26))
    filename=SP${n}_S${m}_L005_R1_001.fastq.gz
    prefix=SP${n}_S${m}_L005
    input=${inputdir}/$filename
    out=${outdir}/$prefix/
    mkdir $out
    $fast -threads 6 --outdir $out $input
done


