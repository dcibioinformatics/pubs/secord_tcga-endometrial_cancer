#!/bin/bash 

fasta=/data1/Annotation/iGenome/Homo_sapiens/NCBI/GRCh38/Sequence/WholeGenomeFasta/genome.fa
star=/opt/NGS/STAR/STAR-2.5.2b/bin/Linux_x86_64_static/STAR
index=/data1/Annotation/iGenome/Homo_sapiens/NCBI/GRCh38/Sequence/STAR/STAR_NCBI-GRCh38
anno=/data1/Annotation/iGenome/Homo_sapiens/NCBI/GRCh38/Annotation/Genes/genes.gtf
#inputdir=/data1/workspace/DCI/secord/RNASeq/Data/Trimmed
inputdir=/data1/workspace/DCI/secord/RNASeq/Data/Reihani_3520_160802A1
outdir=/data1/workspace/DCI/secord/RNASeq/Results/STAR

for n in {2..13}
do
    m=$((${n}+26))
    filename=SP${n}_S${m}_L004_R1_001.fastq.gz
    prefix=SP${n}_S${m}_L004
    input=${inputdir}/$filename
    out=${outdir}/$prefix/
    mkdir $out
    $star --runThreadN 16 \
        --genomeDir ${index} \
        --readFilesIn ${input} \
        --sjdbGTFfile ${anno} \
        --readFilesCommand zcat \
        --quantMode TranscriptomeSAM GeneCounts \
        --clip3pAdapterSeq "GATCGGAAGAGCACACGTCTGAACTCCAGTCAC" \
        --twopassMode Basic \
        --outFileNamePrefix ${out}/$prefix \
        --outSAMtype BAM Unsorted SortedByCoordinate
done

for n in {2..13}
do
    m=$((${n}+26))
    filename=SP${n}_S${m}_L005_R1_001.fastq.gz
    prefix=SP${n}_S${m}_L005
    input=${inputdir}/$filename
    out=${outdir}/$prefix/
    mkdir $out
    $star --runThreadN 16 \
        --genomeDir ${index} \
        --readFilesIn ${input} \
        --sjdbGTFfile ${anno} \
        --readFilesCommand zcat \
        --quantMode TranscriptomeSAM GeneCounts \
        --clip3pAdapterSeq "GATCGGAAGAGCACACGTCTGAACTCCAGTCAC" \
        --twopassMode Basic \
        --outFileNamePrefix ${out}/$prefix \
        --outSAMtype BAM Unsorted SortedByCoordinate
done

