\documentclass[xcolor=x11names,compress]{beamer}
%\usepackage{pgfpages}
%\pgfpagesuselayout{4 on 1}[letterpaper,border shrink=5mm]
%\usepackage{booktabs}
\useinnertheme{circles}
\newenvironment{proenv}{\only{\setbeamercolor{local structure}{fg=green}}}{}
\newenvironment{conenv}{\only{\setbeamercolor{local structure}{fg=red}}}{}
\usepackage{tikz}
\usepackage{verbatim}
\usepackage[font={small, it}]{caption}
\usetikzlibrary{arrows}
\usetikzlibrary{shapes}
%\setbeamersize{text margin left=3pt,text margin right=3pt}
\setbeamertemplate{caption}[numbered]
\input{/data1/workspace/DCI/Sarantopoulos/humanradiation/Preamble/preamble}


\begin{document}

\tikzstyle{pointing} = [pin edge={to-,thin,black}]
\tikzset{
    cross/.style={cross out, draw=black, inner sep=1pt, outer sep=1pt},
}

<<setup,echo=FALSE, include=FALSE>>=
rm(list=ls())
stdt<-date()
set.seed(122333)
options(width = 200, stringsAsFactors=FALSE)
library(knitr)
library(xtable)
opts_chunk$set(
               dev='png',
               comment="",
               dpi=300,
               fig.align='center',
               fig.show='hold',
               size='tiny',
               fig.width=7, fig.height=7,
               out.width='.7\\linewidth')
@ 

%slide1: Project name, author,
\begin{frame}
\title{Secord Ovarian RNAseq Project}
\subtitle{PART I DATA Pre-processing}
\author{DCI Bioinformatics Shared Resource}
\textsc{Version 1.0}
\input{/data1/workspace/DCI/Sarantopoulos/humanradiation/Preamble/kochcurve}\\
\vspace{0.5cm}
\today
\titlepage
%\input{Logo/logos}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%slide 2: Table of contents
\begin{frame}{Outline}
\tableofcontents
\end{frame}

%slide 3:
\tiny

\section{Prerequisite data and packages}
\begin{frame}[fragile]{Prerequisite data and packages}
<<packages, echo=T,results='asis',comment=F,warning=FALSE,cache=FALSE,error=FALSE,message=FALSE>>=
library(dplyr)
library(tidyr)
library(foreach)
library(doParallel)
library(Biobase)
library(reshape2)
library(ggplot2)
library(ShortRead)
library(QuasR)
library(rtracklayer)
library(GenomicFeatures)
library(DESeq)
library(gdata)
library(oligo)
#Dirs
rootdir<-"/data1/workspace/DCI/secord/RNASeq"
fastqdir<-file.path(rootdir, "Data", "Trimmed")
projdir<-file.path(rootdir, "Results", "Bowtie2_hg19")
annodir<-"/data1/Annotation/iGenome/Homo_sapiens/UCSC/hg19"
#annodir<-"/data1/Annotation/iGenome/Homo_sapiens/NCBI/GRCh38/"
fastindex.dir<-file.path(annodir,"Sequence", "Bowtie2Index")
#local functions
source(file.path(rootdir, "Code", "RNASeq.functions.R"))
@ 
\end{frame}

\section{Raw Data sets}
\begin{frame}[fragile]{Annotation and reference files}
<<datasets, echo=T,results='markup',comment="",warning=FALSE,cache=FALSE,error=FALSE,message=FALSE>>=
annofile<-file.path(fastindex.dir,"genome.fa")#reference genome
gff.file<-file.path(annodir,"Annotation", "Genes", "genes.gtf")#gene annotation file
fname=c(list.files(fastindex.dir, pattern="bt2"), basename(gff.file), basename(annofile))
ffname=c(list.files(fastindex.dir, pattern="bt2",full.names=TRUE), gff.file, annofile)
md5sum=as.character(tools::md5sum(ffname))
kable(data.frame(fname,md5sum))
@
\end{frame}

\begin{frame}[fragile]{Trimmed Fastq files}
<<echo=TRUE,results='markup',comment="",warning=FALSE,cache=FALSE,error=FALSE,message=FALSE>>=
dd<-data.frame(fastq=list.files(fastqdir,
            pattern="fastq.gz"),stringsAsFactors=FALSE)
ddf<-data.frame(fastq=list.files(fastqdir,
            pattern="fastq.gz", full.names=TRUE),stringsAsFactors=FALSE)
kable(data.frame(dd$fastq, md5sum=as.character(tools::md5sum(ddf$fastq))))
@
\end{frame}


\begin{frame}[fragile]{Phenotypic data}
<<echo=TRUE,results='markup',comment="",warning=FALSE,cache=FALSE,error=FALSE,message=FALSE>>=
dd$samplename<-gsub("_R1_001-trimmed.fastq.gz", "", dd$fastq)
phdat<-dd
@
\end{frame}

\section{Program versions and settings}

\begin{frame}[fragile]{Program Version}
  \begin{itemize}
  \item tophat2
<<tophat, echo=TRUE, results="markup">>=
tophat="/opt/NGS/tophat/tophat-2.1.1/src/tophat2"
system2(tophat,"-v",stdout=TRUE)
@   
\item bowtie2
<<bowtie2, echo=T, results="markup">>=
bowtie2="/opt/NGS/bowtie2/bowtie2-2.2.9/bowtie2"
system2(bowtie2,"--version",stdout=TRUE)[1]
@ 
\item htseq-count
<<htseqcnt, echo=T, results="markup">>=
htseqcnt="/opt/anaconda2-dev/bin/htseq-count"
system2(htseqcnt,"| tail -n 3",stdout=TRUE)[3]
@ 
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Tophat2 Flags}
<<echo=T, results="markup">>=
th0<-paste("--num-threads 6",#-p, use this many threads to align reads
           "--mate-inner-dist 150",#-r, the expected inner distance between mate pairs (default 50).
           "--mate-std-dev 20",#the standard deviation (default 20).
           "--no-coverage-search",#disable the coverage based search for junctions
           "--transcriptome-max-hits 1", #-x, max number of mappings allowed 
           "--max-multihits 1",#-g, allow up to this many alignments to the reference 
           "--min-intron-length 70",#-i, min intron length, the default is 70.
           "--max-intron-length 500000",#-I, max intron length, the default is 500,000
           "--library-type fr-unstranded",# the default is unstranded (fr-unstranded).
           "--GTF",gff.file)
@   
\end{frame}

\begin{frame}[fragile]{htseq-count flags}
<<echo=TRUE, results="markup">>=
ht0<-paste("--format=bam",
           "--stranded=no", #no specific strand
           "--minaqual=10", #skip all reads with alignment quality lower than # (default 10)
           "--mode=union", #mode to handle reads overlapping more than one feature
           "--type=exon",
           "--idattr=gene_id")
@   
\end{frame}

\begin{frame}[fragile]{Setting list file}
<<echo=TRUE>>=
fastid<-file.path(fastindex.dir, "genome")
spar<-list(tophat=tophat,bowtie2=bowtie2,htseqcnt=htseqcnt,
           tophatcores=4,htseqcores=6,
           th0=th0,ht0=ht0,
           tophatdir="tophat",tophatbam="accepted_hits.bam",
           htseqdir="htseqcnt",
           annofile=annofile,fastindex=fastid,gtf=gff.file,projdir=projdir)
@   
\end{frame}

\section{Data process}

\begin{frame}[fragile]{Create Project and Sample Directories}
<<echo=TRUE, results="asis">>=
out1<-for(i in 1:nrow(phdat)) {
    sampdir=file.path(spar[["projdir"]],phdat[["samplename"]][i])
    dir.create(sampdir)
}
@   
\end{frame}

\begin{frame}[fragile]{Reads Alignment}
 
<<echo=TRUE, results="asis">>=
cl=makeForkCluster(spar[["tophatcores"]])
registerDoParallel(cl)
start<-Sys.time()
proc.time<-NULL
out2=foreach(i=1:nrow(phdat))%dopar%{
    cat(i, "\n")
    starti<-Sys.time()
    fastq<-file.path(fastqdir,phdat[["fastq"]][i])
    sampid<-phdat[["samplename"]][i]
    tophatfun(fastq, sampid, spar,
              fout="tophat.stdout",
              ferr="tophat.stderr")
    proc.time[i]<-Sys.time()-starti
}
print(Sys.time()-start)
stopCluster(cl)
@

\end{frame}

\begin{frame}[fragile]{Alignment summary}
<<echo=TRUE,results='markup',comment="",warning=FALSE,cache=FALSE,error=FALSE,message=FALSE>>=
bfl<-BamFileList(file.path(projdir, phdat[["samplename"]],"tophat", "accepted_hits.bam"),
                                                     yieldSize=50000, index=character())
ubfl<-BamFileList(file.path(projdir, phdat[["samplename"]],"tophat", "unmapped.bam"),
                                                     yieldSize=50000, index=character())
Nalign<-countBam(bfl)
Nunalign<-countBam(ubfl)
read_statsDF<-data.frame(phdat[["samplename"]], Nalign=Nalign$records, 
                        Nunmap=Nunalign$records, sum=Nalign$records+Nunalign$records) 
read_statsDF$Perc_Aligned<-with(read_statsDF, Nalign/sum*100)
colnames(read_statsDF)[1]<-"samplename"
save(read_statsDF, file=file.path(projdir, "Secord_read_statsDF.RData"))
load(file.path(projdir, "Secord_read_statsDF.RData"))
@
\end{frame}

\begin{frame}[fragile]{Alignment summary}
<<echo=F,results='asis',comment="",warning=FALSE,cache=FALSE,error=FALSE,message=FALSE>>=
print(xtable(read_statsDF, caption="Summary of alignment"),caption.placement='top', scalebox=0.8)
@
\end{frame}

\section{Assembly reads to feature}
\begin{frame}[fragile]{Reads assembly to feature using htseq-count}
<<echo=TRUE,results='markup',comment="",warning=FALSE,cache=FALSE,error=FALSE,message=FALSE>>=
cl=makeForkCluster(spar[["tophatcores"]])
registerDoParallel(cl)
startht<-Sys.time()
proc.time<-NULL
out3=foreach(i=1:nrow(phdat))%dopar%{
    starti<-Sys.time()
    bamfile=file.path(projdir,phdat[["samplename"]][i], "tophat", "accepted_hits.bam")
    sampid=phdat[["samplename"]][i]
    htseqcountfun(bamfile, sampid, ferr="ht.stderr", spar)
    proc.time<-rbind(proc.time, Sys.time()-starti)
}
tot.time<-Sys.time()-startht
stopCluster(cl)
@
\end{frame}

\begin{frame}[fragile]{Summary of assembly}
<<echo=T,results='markup',comment="",warning=FALSE,cache=FALSE,error=FALSE,message=FALSE>>=
countlist<-NULL
for (i in phdat[["samplename"]]){
    count<-data.frame(read.table(file.path(projdir, i, "htseqcnt", paste0(i, "-htseq-count.tsv"))
                                    , stringsAsFactors=FALSE))
    rownames(count)<-count[,1]
    colnames(count)<-c("gene", i)
    countlist[[i]]<-count
}
sum(!sapply(countlist, rownames)==rownames(countlist[[1]])) #check the rownames are all identical.
countdf<-sapply(countlist, function(x) x[,2])
rownames(countdf)<-rownames(countlist[[1]])
countdf<-countdf[order(rownames(countdf)),]
htcount<-countdf
save(htcount, file=file.path(projdir, "Secord_countdf_htcount.RData"))
load(file.path(projdir, "Secord_countdf_htcount.RData"))
@
\end{frame}

%\begin{frame}[fragile]{Summary of assembly}
%<<echo=F,results='asis',comment="",warning=FALSE,cache=FALSE,error=FALSE,message=FALSE>>=
%print(xtable(head(htcount, n=15), caption="Read counts by using htseq-count method (15 of 23715)"), scale=0.5)
%@
%\end{frame}

\begin{frame}[fragile]{Summary of assembly}
<<echo=F,results='asis',comment="",warning=FALSE,cache=FALSE,error=FALSE,message=FALSE>>=
htcount<-data.frame(htcount)
ht<-transmute(htcount,
           SP2=SP2_S28_L004+SP2_S28_L005,
           SP3=SP3_S29_L004+SP3_S29_L005,
           SP4=SP4_S30_L004+SP4_S30_L005,
           SP5=SP5_S31_L004+SP5_S31_L005,
           SP6=SP6_S32_L004+SP6_S32_L005,
           SP7=SP7_S33_L004+SP7_S33_L005,
           SP8=SP8_S34_L004+SP8_S34_L005,
           SP9=SP9_S35_L004+SP9_S35_L005,
           SP10=SP10_S36_L004+SP10_S36_L005,
           SP11=SP11_S37_L004+SP11_S37_L005,
           SP12=SP12_S38_L004+SP12_S38_L005,
           SP13=SP13_S39_L004+SP13_S39_L005
           )
cap="Read counts for each sample by using htseq-count method (15 of 23715)"
print(xtable(head(ht, n=15), caption=cap), scale=0.7)
@
\end{frame}

\section{Session Information}
\begin{frame}[fragile]{Session Information}
<<echo=FALSE,results='asis'>>=
toLatex(sessionInfo(), locale=FALSE)
@ 

<<echo=FALSE>>=
print(paste("Start Time",stdt))
print(paste("End Time  ",date()))
@ 
\end{frame}
\end{document}
