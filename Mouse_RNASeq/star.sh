#!/bin/bash 

star=/opt/NGS/STAR/STAR-2.5.2b/bin/Linux_x86_64_static/STAR
index=/data1/Staging/STAR/RNA-Seq/STARindex/Mus_musculus/mm10/UCSC
anno=/data1/Staging/STAR/RNA-Seq/STARindex/Mus_musculus/mm10/UCSC/genes.gtf
inputdir=/data1/workspace/DCI/secord/Mouse_RNASeq/Data/3k0i8veznijs49om0gcpez61w3plnr7r3bvlhvwzpmskctlqm1
outdir=/data1/workspace/DCI/secord/Mouse_RNASeq/Results/star_output

for g in H L
do
    for m in C1 C2 C3 C4 C5 T1 T2 T3 T4 T5
    do
        for l in {1..3}
        do
            filename1=MET_${g}_${m}_L00${l}_R1_001.fastq.gz
            filename2=MET_${g}_${m}_L00${l}_R2_001.fastq.gz
            echo "now processing $filename1 and $filename2"
            prefix=MET_${g}_${m}_L00${l}
            input1=${inputdir}/$filename1
            input2=${inputdir}/$filename2
            out=${outdir}/$prefix
            mkdir $out 
            chmod 770 $out

            $star --runThreadN 16 \
                --genomeDir ${index} \
                --readFilesIn ${input1} ${input2}\
                --sjdbGTFfile ${anno} \
                --readFilesCommand zcat \
                --quantMode TranscriptomeSAM GeneCounts \
                --twopassMode Basic \
                --outFileNamePrefix ${out}/$prefix \
                --outSAMtype BAM Unsorted SortedByCoordinate
        done
    done
done

