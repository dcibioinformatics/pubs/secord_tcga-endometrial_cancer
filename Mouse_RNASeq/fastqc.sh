#!/bin/bash 

#fast=/opt/NGS/FastQC/0.11.5/FastQC/fastqc
fast = /opt/Anaconda2-4.3.0/FastQC/fastqc
inputdir=/data1/workspace/DCI/secord/Mouse_RNASeq/Data/3k0i8veznijs49om0gcpez61w3plnr7r3bvlhvwzpmskctlqm1
outdir=/data1/workspace/DCI/secord/Mouse_RNASeq/QC/rawQC
$fast --threads 6 \
 --outdir $outdir \
 $inputdir/*.fastq.gz



