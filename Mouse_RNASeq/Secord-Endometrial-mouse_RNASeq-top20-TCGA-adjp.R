##Mouse RNASeq results High vs Low within control
library(tidyverse)
library(qvalue)
wd<-"/data1/workspace/DCI/secord/Mouse_RNASeq"
mres<-read.csv(file.path(wd, "/Results/Secord_Endometrial_mouseRNASeq_Diet_within_Control.csv"), 
               head=TRUE, 
               stringsAsFactors=FALSE)
mtop20<-toupper(mres$Gene[1:20])
mtop20<-ifelse(mtop20=="ANG4", "ANG", mtop20)
hres<-read.csv(file.path(wd, "/Data/Genelist/Cobb_TCGA_Results_adjusted_penaltyreg.csv"), 
               head=TRUE,
               stringsAsFactors=FALSE)%>%
        mutate(Gene=sapply(X, function(g) strsplit(g, "\\|")[[1]][1]))
shres<-filter(hres, Gene%in%mtop20)%>%
    arrange(p.value)%>%
    select(Genes=X, Est, Stderror, t.value, p.value)
shres$q.value<-qvalue(shres$p.value)$qvalues
write.csv(shres, file=file.path(wd, "Results", "Secord_Endometrial_top20.mouse.RNASeq_in_TCGA.csv"),
        quote=F)
