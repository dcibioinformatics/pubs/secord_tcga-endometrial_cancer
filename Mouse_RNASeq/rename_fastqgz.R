options(width=250)
library(tidyverse)
dir<-"/data1/workspace/DCI/secord/Mouse_RNASeq"
mf<-read.delim(file.path(dir, "Data/VBJ_160920_UNC32-K00270_0025_BHFTTYBBXX.txt"), 
               head=TRUE, stringsAsFactors=FALSE)

suffix<-sapply(mf$FILE_NAME, function(x)
               substr(x, nchar(x)-20, nchar(x)))
mf<-mutate(mf, FILE.NAME.LOCAL=paste0(SAMPLE_NAME, suffix))
length(unique(mf$FILE.NAME.LOCAL))==nrow(mf)
setwd(dirname(files))
file.rename(from=mf$FILE_NAME, to=mf$FILE.NAME.LOCAL)

