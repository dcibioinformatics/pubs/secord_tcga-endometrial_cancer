
\documentclass[xcolor=x11names,compress]{beamer}
%\usepackage{pgfpages}
%\pgfpagesuselayout{4 on 1}[letterpaper,border shrink=5mm]
%\usepackage{booktabs}
\useinnertheme{circles}
\newenvironment{proenv}{\only{\setbeamercolor{local structure}{fg=green}}}{}
\newenvironment{conenv}{\only{\setbeamercolor{local structure}{fg=red}}}{}
\usepackage{tikz}
\usepackage{verbatim}
\usepackage{multirow}
\usepackage{graphicx}
\usetikzlibrary{arrows}
\usetikzlibrary{shapes}
%\setbeamersize{text margin left=3pt,text margin right=3pt}

\input{/data1/workspace/DCI/Sarantopoulos/humanradiation/Preamble/preamble}

\begin{document}

\tikzstyle{pointing} = [pin edge={to-,thin,black}]
\tikzset{
    cross/.style={cross out, draw=black, inner sep=1pt, outer sep=1pt},
}

<<setup,echo=FALSE, include=FALSE>>=
rm(list=ls())
stdt<-date()
set.seed(122333)
options(width = 200, stringsAsFactors=FALSE)
library(knitr)
library(xtable)
library(lattice)
library(reshape2)
library(ggplot2)
library(RColorBrewer)
opts_chunk$set(comment="",out.height="0.8\\textheight",dev="png",dpi=300,
               out.extra='keepaspectratio',
               warning=FALSE,cache=FALSE,message=FALSE, fig.align="center"
               )
set.seed(1123)
@ 

%slide1: Project name, author,
\begin{frame}
\title{TCGA UCEC RNAseq Data Analysis}
\subtitle{PART III Model comparison and DATA Analysis}
\author{DCI Bioinformatics Shared Resource}
%\textsc{QC by Dadong Zhang}
\input{/data1/workspace/DCI/Sarantopoulos/humanradiation/Preamble/kochcurve}\\
\vspace{0.5cm}
%\today
\titlepage
%\input{Logo/logos}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%slide 2: Table of contents
%\begin{frame}[fragile]{Outline}
%\tableofcontents
%\end{frame}

\tiny
\begin{frame}[fragile]{Load Packages}
<<echo=TRUE,message=FALSE,warning=FALSE>>=
wd<-"/data1/workspace/DCI/secord"
source("/data1/workspace/DCI/dz33/RNAseq/RNAseq_functions_1.0.R")
library(DESeq2)
library(limma)
library(Kendall)
library(qvalue)
load(file.path(wd, "Data", "Secord_TCGA_clindat.RData"))
load(file.path(wd, "Data", "Secord_TCGA_expdat.RData"))
load(file.path(wd, "Data", "Secord_TCGA_DESeq2_ddsobj_BMI.RData"))
clindat<-clindat[["clindat_study"]]
clin.dat<-clindat[!is.na(clindat$BMI), ]
@ 
\end{frame}

<<>>=
check cgdsr RNASeqV2 and downloaded RNASeqV2
library(cgdsr)
    mycgds = CGDS("http://www.cbioportal.org/")
# Get available case lists (collection of samples) for a given cancer study
    mycancerstudy = getCancerStudies(mycgds)[121,1]
    mycaselist = getCaseLists(mycgds,mycancerstudy)[7, 1]
# Get available genetic profiles
    mygeneticprofile = getGeneticProfiles(mycgds,mycancerstudy)[7,1]
# Get data slices for a specified list of genes, genetic profile and case list
    test<-getProfileData(mycgds,c('TGFA', 'BRCA1'),mygeneticprofile,mycaselist)
    local
names(expdat.all) 
    loc<-expdat.all[["rsem.genes.results"]]
    lf<-t(loc[grep("TGFA|BRCA1", rownames(loc)), ])
    test1<-lf[rownames(test), ]
# 
    try TCGAbiolinks
library(TCGAbiolinks)
    sam<-rownames(clin.dat)[1:3]
    tq<-TCGAquery(tumor="UCEC", platform="IlluminaGA_RNASeqV2", samples=sam, level="3")
    td<-TCGAdownload(data=tq, path=file.path(wd, "Data"), type="rsem.genes.results")
file.TCGAbiolinks<-list.files(file.path(wd, "Data", "unc.edu_UCEC.IlluminaGA_RNASeqV2.Level_3.1.0.0"))
sum(clin.dat$rsem.genes.results%in%file.TCGAbiolinks)==nrow(clin.dat)
@




\begin{frame}[fragile]{Normalization}
<<echo=F, results="asis", message=FALSE,warning=FALSE>>=
#TCGA normalized data
gnorm.dat<-expdat.all[["rsem.genes.normalized_results"]]
#local upper quartile noralization
graw.dat<-expdat.all[["rsem.genes.results"]]
gnorm.locdat<-quartile_normalization(graw.dat)
#comparison
#DESeq2 normalization by rounding to intergers
graw.intdat<-round(graw.dat, 0)
colData<-clin.dat
#dds<-DESeqDataSetFromMatrix(countData=graw.intdat,
#                            colData=colData,
#                            design=~BMI)
#dds <- estimateSizeFactors(dds)
#dds <- estimateDispersions(dds)
ddsnorm.dat<-counts(dds, normalized=TRUE)
#plot
set.seed(12435)
x<-sample(rownames(gnorm.dat[rowSums(gnorm.dat)>0, ]), 4)
par(mfrow=c(2,2))
for(i in x){
plot(gnorm.locdat[i,], gnorm.dat[i,], xlab="Local manual normalization", ylab="Other normalization", col=c(2,3), cex=1.5)
points(gnorm.locdat[i,], ddsnorm.dat[i,], col=4, cex=1.5)
legend("topleft",legend=c("Local manual", "TCGA RSEM", "DESeq2"),  col=c(2,3,4), pch=1)
}
@ 
\end{frame}

\section{DESeq2}
\begin{frame}[fragile]{Analysis uisng DESeq2}
<<echo=F, results="asis", message=FALSE,warning=FALSE>>=
#dds<-DESeq(dds)
#save(dds, file=file.path(wd, "Data", "Secord_TCGA_DESeq2_ddsobj_BMI.RData"))
resdeseq2<-results(dds)
resdeseq2.ord<-data.frame(resdeseq2[order(resdeseq2$padj), ])
nrow(resdeseq2.ord[resdeseq2.ord$padj<0.05, ])
tab<-resdeseq2.ord[1:20,]
tab$pvalue<-formatC(tab$pvalue, digits=2, format="e")
tab$padj<-formatC(tab$adj, digits=2, format="e")
cap<-"TCGA DEA of BMI and RNASeqV2 using DESeq2"
print(xtable(tab, digits=2, caption=cap), caption.placement="top")
@ 
\end{frame}

\begin{frame}[fragile]{Top Gene}
<<echo=F, results="asis", message=FALSE,warning=FALSE>>=
x<-rownames(resdeseq2.ord)
xx<-x[1]
fit<-lm(colData$BMI~gnorm.dat[xx,])
ggplotRegression(fit, xlab="BMI", ylab=xx)
@ 
\end{frame}

\begin{frame}[fragile]{Gene TGFA}
<<echo=F, results="asis", message=FALSE,warning=FALSE>>=
xx<-x[grepl("TGFA", x)]
fit<-lm(colData$BMI~gnorm.dat[xx,])
ggplotRegression(fit, xlab="BMI", ylab=xx)
@ 
\end{frame}

\section{limma}
\begin{frame}[fragile]{Analysis using limma}
<<echo=F, results="asis", message=FALSE,warning=FALSE>>=
#dds<-DESeq(dds)
#save(dds, file=file.path(wd, "Data", "Secord_TCGA_DESeq2_ddsobj_BMI.RData"))
gnorm.logdat<-log2(gnorm.dat+1)
design<-model.matrix(~colData$BMI)
fit<-lmFit(gnorm.logdat, design)
fit<-eBayes(fit)
res.limma<-topTable(fit, coef=2, number=Inf)
nrow(res.limma[res.limma$adj.P.Val<0.05, ])
tab<-res.limma[1:20,]
tab$P.Value<-formatC(tab$P.Value, digits=2, format="e")
tab$adj.P.Val<-formatC(tab$adj.P.Val, digits=2, format="e")
cap<-"TCGA DEA of BMI and RNASeqV2 using limma"
print(xtable(tab, digits=2, caption=cap), caption.placement="top")
@ 
\end{frame}

\begin{frame}[fragile]{Top Gene}
<<echo=F, results="asis", message=FALSE,warning=FALSE>>=
x<-rownames(res.limma)
xx<-x[1]
fiti<-lm(colData$BMI~gnorm.logdat[xx,])
ggplotRegression(fiti, xlab="BMI", ylab=xx)
@ 
\end{frame}

\begin{frame}[fragile]{Gene TGFA}
<<echo=F, results="asis", message=FALSE,warning=FALSE>>=
xx<-x[grepl("TGFA", x)]
fiti<-lm(colData$BMI~gnorm.logdat[xx,])
ggplotRegression(fiti, xlab="BMI", ylab=xx)
@ 
\end{frame}

\section{Kandall}
\subsection{All genes}
\begin{frame}[fragile]{Analysis using Kandall Tau}
<<echo=F, results="asis", message=FALSE,warning=FALSE>>=
res.tau<-NULL
gnorm.logdat<-gnorm.logdat[rowSums(gnorm.logdat)>0, ]
for(g in rownames(gnorm.logdat)){
    res.tau<-rbind.data.frame(res.tau, c(g, cor.kendall(gnorm.logdat[g, ], colData$BMI)))
}
colnames(res.tau)<-c("gene", "tau", "score", "D", "varS", "p.value")
res.tau<-rowbycol(res.tau, 1)
res.kendall<-data.frame(apply(res.tau, 2, as.numeric))
rownames(res.kendall)<-rownames(res.tau)
res.kendall<-res.kendall[order(res.kendall$p.value), ]
res.kendall$q.value<-qvalue(res.kendall$p.value)$qvalue
nrow(res.kendall[res.kendall$q.value<0.05, ])
tab<-res.kendall[1:20,]
tab$p.value<-formatC(tab$p.value, digits=2, format="e")
tab$q.value<-formatC(tab$q.value, digits=2, format="e")
cap<-"TCGA DEA of BMI and RNASeqV2 using Kendall tau correlation test"
print(xtable(tab, digits=2, caption=cap), caption.placement="top")

save(res.kendall, file=file.path(wd, "Data", "Secord_TCGA_results_kendall.RData"))
write.table(res.kendall, file=file.path(wd, "Data", "Secord_TCGA_results_kendall.txt"), sep="\t", quote=F)
@ 
\end{frame}

\begin{frame}[fragile]{Top Gene}
<<echo=F, results="asis", message=FALSE,warning=FALSE>>=
x<-rownames(res.kendall)
xx<-x[1]
fiti<-lm(colData$BMI~gnorm.logdat[xx,])
ggplotRegression(fiti, xlab="BMI", ylab=xx)
@ 
\end{frame}

\begin{frame}[fragile]{Gene TGFA}
<<echo=F, results="asis", message=FALSE,warning=FALSE>>=
xx<-x[grepl("TGFA", x)]
fiti<-lm(colData$BMI~gnorm.logdat[xx,])
ggplotRegression(fiti, xlab="BMI", ylab=xx)
@ 
\end{frame}

\begin{frame}[fragile]{Appendix.}
<<echo=FALSE,results='asis'>>=
toLatex(sessionInfo(), locale=FALSE)
@ 
<<echo=FALSE>>=
print(paste("Start Time",stdt))
print(paste("End Time  ",date()))
@ 
\end{frame}

\end{document}



