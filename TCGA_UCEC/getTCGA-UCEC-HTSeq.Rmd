---
title: "TCGA-UCEC RNA-Seq (HTSeq counts) query"
author: "DCI Bioinformatics"
date: "7/19/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
This script was written to retrieve the TCGA-UCEC RNA-Seq counts
to address questions by the reviewers.

# Load Packages
```{r pkgs}
library(DESeq2)
library(dplyr)
library(TCGAbiolinks)
```

# Query data
```{r query}
query <- TCGAbiolinks::GDCquery(
  project = "TCGA-UCEC",
  data.category = "Transcriptome Profiling",
  data.type = "Gene Expression Quantification",
  workflow.type = "HTSeq - Counts"
)

TCGAbiolinks::GDCdownload(query)


gdat <- TCGAbiolinks::GDCprepare(query)
```

# Save data
```{r savedat}
wd <- "Data/"

if (!dir.exists(wd)) {
  dir.create(wd)
}


outfile <- file.path(wd, "TCGA_UCEC_Expressionset.RDS")
saveRDS(gdat, file = outfile)

tools::md5sum(outfile)
```

# Filter data
```{r procdat}
dim(gdat)
# primary tumor only/remove normal
dds_tp <- gdat[, gdat$shortLetterCode == "TP"]
dim(dds_tp)
# Remove duplicate pts ids
dds_tp_unq <- dds_tp[, !duplicated(dds_tp$patient)]
dim(dds_tp_unq)
# Remove cases with missing BMI
dds_tp_unq_bmi <- dds_tp_unq[, !is.na(dds_tp_unq$bmi)]
dim(dds_tp_unq_bmi)
# Summary of BMI (before subsetting by CNH status)
summary(dds_tp_unq_bmi$bmi)
# Tabulate integrative cluster (not that previously this variable was "subtype_IntegrativeCluster"
table(dds_tp_unq_bmi$paper_IntegrativeCluster, exclude = NULL)
# Create DESeq2 object for those classified as CN high
dds_tp_unq_bmi_cnh <- dds_tp_unq_bmi[, which(dds_tp_unq_bmi$paper_IntegrativeCluster == "CN high")]
dds_cnh <- DESeq2::DESeqDataSet(dds_tp_unq_bmi_cnh, design = ~ log(bmi))
dim(dds_cnh)
```

```{r basicDE}
ddsres <- DESeq2::DESeq(dds_cnh)
DESeq2::results(ddsres, tidy = TRUE) %>%
  dplyr::arrange(pvalue) %>%
  head(10)
```

# Session information
```{r sessinfo}
sessionInfo()
```
